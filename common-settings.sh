set -e
set -x
DOCKER_ORGANISATION=asciidoctor
DOCKER_PROJECT="testing-lab"
BASE=$(pwd)

if [[ "${CI_COMMIT_REF_NAME}" == 'master' ]] ; then
	DOCKER_TAG="${CI_COMMIT_SHA}"
else
	DOCKER_TAG="${CI_COMMIT_REF_NAME}"
fi

