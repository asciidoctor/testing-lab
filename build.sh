#!/bin/sh

. common-settings.sh

for proj in $(cat containers.txt) ; do

  DOCKER_IMAGE="${proj}"
  CONTAINER_TEST_IMAGE="${CI_REGISTRY}/${DOCKER_ORGANISATION}/${DOCKER_PROJECT}/${DOCKER_IMAGE}:${DOCKER_TAG}"

  docker build --pull -t ${CONTAINER_TEST_IMAGE} $(pwd)/${proj}

  if [[ -x test.sh ]] ; then
	  ./test.sh
  fi

  if [[ "${CI_COMMIT_REF_NAME}" == "master" ]] ; then
	docker push ${CONTAINER_TEST_IMAGE}
  fi

done

